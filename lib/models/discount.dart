import 'beer.dart';

class Discount {
  String pathToImg;
  Beer beer;

  Discount(this.pathToImg, this.beer);
}

List<Discount> discounts = [
  Discount('assets/beer_img/beer(1).jpg', new Beer(name: 'Okocim', price: 2.09)),
  Discount('assets/beer_img/beer(2).jpg', new Beer(name: 'Łomża', price: 2.69)),
  Discount('assets/beer_img/beer(3).jpg', new Beer(name: 'Zatecky', price: 3.10)),
  Discount('assets/beer_img/beer(4).jpg', new Beer(name: 'Żywiec', price: 3.00)),
  Discount('assets/beer_img/beer(5).jpg', new Beer(name: 'Guiness', price: 4.49)),
  Discount('assets/beer_img/beer(6).jpg', new Beer(name: 'Budweiser', price: 3.69)),
  Discount('assets/beer_img/beer(7).jpg', new Beer(name: 'Wojskowe', price: 1.40)),
  Discount('assets/beer_img/beer(8).jpg', new Beer(name: 'Hopfee', price: 1.99)),
  Discount('assets/beer_img/beer(9).jpg', new Beer(name: 'Miłosław', price: 3.49)),
];