import 'package:collection/collection.dart';
import 'dart:ffi';

import '../constants.dart';
import '../db.dart';

class Account {
  final String login;
  final String password;
  Account({ required this.login, required this.password});

  /// Returns Account or if auth failed returns null
  Account? auth(String login, String password) {
    return accounts.firstWhereOrNull((element) => element.login == login && element.password == password);
  }

}

