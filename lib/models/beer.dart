import 'package:collection/collection.dart';

import '../db.dart';

class Beer {
  String name;
  double price;
  int communityVotes;

  Beer({required this.name, required this.price, this.communityVotes = 0});
}

class Store {
  String name;
  String address;
  String navigationAddress;
  List<Beer> beers = [];

  Store({
    required this.name,
    required this.address,
    required this.navigationAddress,
    required this.beers,
  });
}

class GoodBeer {
  late Beer? beer;
  // Query
  String beerName = "";
  String storeName = "";
  //
  Store? store;

  GoodBeer(this.beerName, this.storeName);

  GoodBeer fetchMembers(GoodBeer item) {
    GoodBeer tmp = item;
    tmp.store = stores.firstWhereOrNull((element) => element.name == storeName);
    tmp.beer = store?.beers.firstWhere((element) => element.name == beerName);
    return tmp;
  }
}

/// Returns all Beers
List<Beer> getAllBeers(List<Store> storesContext) {
  List<Beer> beers = [];
  for (int i = 0; i < storesContext.length; i++) {
    beers.addAll(storesContext[i].beers);
  }
  return beers;
}

List<Beer> getAllBeersDesc(List<Store> storesContext) {
  List<Beer> beers = getAllBeers(storesContext);
  beers.sort((a,b) => b.communityVotes.compareTo(a.communityVotes));
  return beers;
}
