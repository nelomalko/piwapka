// Font styles
import 'dart:ui';

import 'package:flutter/material.dart';

TextStyle h1 = TextStyle(fontSize: 30, fontWeight: FontWeight.w500, fontFamily: 'Poppins', color: Colors.grey.shade700);
TextStyle h2 = TextStyle(fontSize: 25, fontWeight: FontWeight.w500, fontFamily: 'Poppins', color: Colors.grey.shade700);
TextStyle hb = TextStyle(fontSize: 10, fontWeight: FontWeight.w500, fontFamily: 'Poppins', color: Colors.grey.shade100);

Color textColor = Colors.grey.shade700;

String disclaimer = 'Nasza aplikacja to demo techniczne i proof of concept zgodne z najnowszymi standardami. Zbudowana modularnie, umożliwia swobodne rozszerzanie funkcjonalności i potencjalne połączenie z API. To przełomowe rozwiązanie, które prezentuje nasze umiejętności i gotowość do wyzwań technologicznych.';
List<String> technologie = [
  "- Modularność",
  "- Niezależność komponentów",
  "- Możliwość integracji zewnętrznej",
  "- Optymalizacja (Odpowiednie zarządzanie stanem)",
  "- Zastosowanie wzorca 'MVC'"
];