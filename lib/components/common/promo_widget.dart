import 'package:beer_finder/models/discount.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

class PromoWidget extends StatelessWidget {
  final Discount discount;
  const PromoWidget({Key? key, required this.discount}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white, border: Border.all(color: Colors.grey, width: 2)),
      width: double.infinity,
      child: Column(
        children: [
          Image.asset(
            discount.pathToImg,
            fit: BoxFit.fitWidth,
            height: 150,
            width: double.infinity,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  discount.beer.name,
                  style: const TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 25,
                      fontWeight: FontWeight.w700),
                  textAlign: TextAlign.left,
                ),
              ),
              Text(NumberFormat.simpleCurrency( locale: "pl_PL").format(discount.beer.price),
                  style: const TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 20,
                      fontWeight: FontWeight.w500))
            ],
          ),
        ],
      ),
    );
  }
}
