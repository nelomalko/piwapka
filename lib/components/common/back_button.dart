import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BackButton extends StatelessWidget {
  const BackButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        elevation: 0,
        heroTag: 'back',
        onPressed: () => {Navigator.pop(context)},
        backgroundColor: Colors.grey.shade200,
        child: SizedBox(
          width: 40,
          height: 40,
          child: Image.asset('assets/back-arrow.png'),
        ));
  }
}
