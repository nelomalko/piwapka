import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BigButton extends StatelessWidget {
  final Color color;
  final String mainText;
  final String secondaryText;
  final Image? logo;
  final Function()? onPressed;

  const BigButton(
      {Key? key,
      required this.color,
      required this.mainText,
      required this.secondaryText,
      this.logo, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: color,
          minimumSize: const Size.fromHeight(100),
          elevation: 0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      onPressed: onPressed,
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(mainText,
                  style: const TextStyle(fontFamily: 'Poppins', fontSize: 20)),
              Text(secondaryText,
                  style: const TextStyle(fontFamily: 'Poppins', fontSize: 15))
            ]),
            Container(
              child: logo,
            )
          ],
        ),
      ),
    );
  }
}
