import 'package:beer_finder/models/beer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';

import '../../constants.dart';

class PlacePicker extends StatelessWidget {
  final GoodBeer goodBeerInstance;

  const PlacePicker({Key? key, required this.goodBeerInstance})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = const TextStyle(
        fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 20);
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
      decoration: BoxDecoration(
          color: Color(0xFFF0F0F0), borderRadius: BorderRadius.circular(15)),
      child: Container(
          padding: const EdgeInsets.all(10),
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      goodBeerInstance.storeName,
                      style: textStyle,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      goodBeerInstance.store!.address,
                      style: const TextStyle(
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.w400,
                          fontSize: 15),
                      textAlign: TextAlign.left,
                    )
                  ],
                ),
                Container(
                  height: 50,
                  width: 50,
                  child: Material(
                borderRadius: BorderRadius.circular(50),
                child: InkWell(
                  onTap: () { MapsLauncher.launchQuery(goodBeerInstance.store!.address); },
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    alignment: Alignment.center,
                    child: Container(
                        child: Image.asset('assets/navigate.png')),
                  ),
                ),
                  ),
                )
              ],
            ),
          )),
    );
  }
}
