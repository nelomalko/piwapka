import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountButton extends StatelessWidget {
  const AccountButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        elevation: 0,
        heroTag: 'account',
        onPressed: () => {},
        backgroundColor: Colors.grey.shade300,
        child: SizedBox(
          width: 40,
          height: 40,
          child: Image.asset('assets/account.png'),
        ));
  }
}
