import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BeerWidget extends StatefulWidget {
  final String beerName;
  final double beerPrice;
  int communityVotes = 0;

  BeerWidget(
      {Key? key,
      required this.beerName,
      required this.beerPrice, required this.communityVotes})
      : super(key: key);

  @override
  State<BeerWidget> createState() => _BeerWidgetState();
}

class _BeerWidgetState extends State<BeerWidget> {
  bool clicked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.amber.shade100,
            borderRadius: BorderRadius.circular(10)),
        margin: const EdgeInsets.all(8),
        padding: const EdgeInsets.all(15),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Text(widget.beerName,
                style: const TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 15,
                    fontWeight: FontWeight.w500)),
            Text('Cena: ${widget.beerPrice} zł',
                style: const TextStyle(fontFamily: 'Poppins', fontSize: 12))
          ]),
          Wrap(
            spacing: 10,
            children: [
              Text(widget.communityVotes.toString(),
                  style: TextStyle(fontFamily: 'Poppins', fontSize: 20)),
              GestureDetector(
                  onTap: (){
                    clicked = !clicked;
                    setState(() {
                      widget.communityVotes++;
                    });
                  },
                  child: Image.asset(
                      clicked ? 'assets/thumb_up2.png' : 'assets/thumb_up.png'))
            ],
          ),
        ]));
  }
}
