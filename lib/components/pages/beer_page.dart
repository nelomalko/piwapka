import 'package:beer_finder/components/common/beer_widget.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';

import '../../db.dart';
import '../../models/beer.dart';

class BeerPage extends StatefulWidget {
  const BeerPage({Key? key}) : super(key: key);

  @override
  State<BeerPage> createState() => _BeerPageState();
}

class _BeerPageState extends State<BeerPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children:
            getAllBeersDesc(stores).map((e) => BeerWidget(beerName: e.name, beerPrice: e.price, communityVotes: e.communityVotes))
                .toList()
          ,
        ),
      ),
    );
  }
}