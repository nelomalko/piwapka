import 'package:beer_finder/components/common/promo_widget.dart';
import 'package:beer_finder/models/discount.dart';
import 'package:flutter/cupertino.dart';

class DiscountPage extends StatefulWidget {
  const DiscountPage({Key? key}) : super(key: key);

  @override
  State<DiscountPage> createState() => _DiscountPageState();
}

class _DiscountPageState extends State<DiscountPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Column(children:
            discounts.map((e) => PromoWidget(discount: e)).toList()
            )
          ]
        ),
      ),
    );
  }
}
