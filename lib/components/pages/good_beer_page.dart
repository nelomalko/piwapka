import 'dart:math';

import 'package:beer_finder/models/beer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../../db.dart';
import '../common/account_button.dart';
import '../common/back_button.dart' as BackButton;
import '../common/place_picker.dart';

class GoodBeerPage extends StatelessWidget {
  const GoodBeerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _random = Random();
    GoodBeer goodBeer = goodBeers[_random.nextInt(goodBeers.length)];
    goodBeer = goodBeer.fetchMembers(goodBeer);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        child: SafeArea(
          child: Column(
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  BackButton.BackButton(),
                  AccountButton(),
                ],
              ),
              Container(
                  alignment: Alignment.topLeft,
                  child: Text('Wylosowane piwo', style: h1)),
              Flexible(
                child: Container(
                  margin: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                  height: 80,
                  decoration: BoxDecoration(
                      color: Colors.amber.shade500,
                      borderRadius: BorderRadius.circular(15)),
                  child: Container(
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      child: Text(
                        goodBeer.beerName,
                        style: const TextStyle(fontFamily: 'Poppins', fontSize: 23, color: Colors.white),
                        textAlign: TextAlign.center,
                      )),
                ),
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Liczba głosów: ${goodBeer.beer?.communityVotes}',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w400,
                            fontSize: 20)),
                    Text('Cena: ${goodBeer.beer?.price} zł',
                        textAlign: TextAlign.left,
                        style: const TextStyle(
                            color: Colors.black54,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w600,
                            fontSize: 20)),
                  ],
                ),
              ),
              Container(
                  alignment: Alignment.topLeft,
                  child: Text('Miejsce:', style: h2)),
              PlacePicker(goodBeerInstance: goodBeer),
            ],
          ),
        ),
      ),
    );
  }
}
