import 'package:beer_finder/components/common/account_button.dart';
import 'package:beer_finder/components/common/big_button.dart';
import 'package:beer_finder/components/pages/good_beer_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Column(
        children: [
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Container(), AccountButton()],
          ),
          Container(
              alignment: Alignment.topLeft,
              child: Text('Czego szukasz?', style: h1)),
          const SizedBox(height: 10),
          Wrap(
            spacing: 20,
            runSpacing: 20,
            children: [
              BigButton(
                mainText: 'Dobre piwo',
                secondaryText: 'Znajdź najlepsze piwo w okolicy',
                color: Colors.amber.shade500,
                logo: Image.asset('assets/beer.png'),
                onPressed: () => {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const GoodBeerPage()))
                },
              ),
              BigButton(
                mainText: 'Popularne',
                secondaryText: 'Bo dlaczego pić samemu?',
                color: Colors.blueGrey,
                logo: Image.asset('assets/popular.png'),
              ),
            ],
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Container(
                    padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: Column(
                      children: [
                        Column(
                          children: [
                            Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, width: 1.5),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                children: [
                                  Container(
                                    margin:
                                        const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                    child: const Text('Informacja o aplikacji',
                                        style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontWeight: FontWeight.w500)),
                                  ),
                                  Text(disclaimer,
                                      style: const TextStyle(
                                          fontFamily: 'Poppins'),
                                      textAlign: TextAlign.justify),
                                ],
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Container(
                              alignment: Alignment.center,
                              padding: const EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                      color: Colors.grey, width: 1.5),
                                  borderRadius: BorderRadius.circular(10)),
                              child: Column(
                                children: [
                                  const Text(
                                      'Elementy charakterystyczne dla naszego projektu:',
                                      style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontWeight: FontWeight.w500),
                                      textAlign: TextAlign.justify),
                                  Container(
                                    margin:
                                        const EdgeInsets.fromLTRB(0, 10, 0, 0),
                                    child: Column(
                                        children: technologie
                                            .map((e) => Padding(
                                                  padding:
                                                      const EdgeInsets.all(2.0),
                                                  child: Text(e),
                                                ))
                                            .toList()),
                                  )
                                ],
                              ),
                            ),
                          ],
                        )
                      ],
                    )),
              ),
            ),
          )
        ],
      ),
    );
  }
}
