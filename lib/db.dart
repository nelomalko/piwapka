import 'models/accounts.dart';
import 'models/beer.dart';

List<Account> accounts = [
  Account(login: 'admin', password: 'admin'),
  Account(login: 'wojtek', password: '1234'),
  Account(login: 'luki', password: '12345678')
];

List<Store> stores = [
  Store(
    name: "Piwoteka Warszawska",
    address: "ul. Poznańska 7, Warszawa",
    navigationAddress: "Poznańska 7, Warszawa",
    beers: [
      Beer(name: "Wiedźmin IPA", price: 5.49, communityVotes: 10),
      Beer(name: "Piwo Smoka Stout", price: 6.99, communityVotes: 5),
      Beer(name: "Złoty Pociąg Pilsner", price: 4.99, communityVotes: 20),
    ],
  ),
  Store(
    name: "Browarium Sklepowe",
    address: "ul. Łucka 11, Warszawa",
    navigationAddress: "Łucka 11, Warszawa",
    beers: [
      Beer(
          name: "Kamień Filozoficzny Witbier", price: 6.49, communityVotes: 15),
      Beer(name: "Chmielowa Rzeka APA", price: 4.99, communityVotes: 12),
      Beer(name: "Jabłko Adamowe Cider", price: 5.99, communityVotes: 8),
    ],
  ),
  Store(
    name: "Kufle i Kapsle",
    address: "ul. Nowogrodzka 25A, Warszawa",
    navigationAddress: "Nowogrodzka 25A, Warszawa",
    beers: [
      Beer(
          name: "Koziołek Matołek Belgian Strong Ale",
          price: 7.49,
          communityVotes: 25),
      Beer(name: "Ciemne Imperium Porter", price: 6.99, communityVotes: 18),
      Beer(name: "Gorzka Wódka IPA", price: 5.49, communityVotes: 14),
    ],
  ),
  Store(
    name: "PiwPaw",
    address: "ul. Wilcza 29, Warszawa",
    navigationAddress: "Wilcza 29, Warszawa",
    beers: [
      Beer(name: "Kamikaze Górnika Rye IPA", price: 6.99, communityVotes: 22),
      Beer(name: "Polska Mocna Lager", price: 4.99, communityVotes: 17),
      Beer(name: "Cydr Świętokrzyski", price: 5.49, communityVotes: 9),
    ],
  ),
  Store(
    name: "Jabeerwocky",
    address: "ul. Hoża 51A, Warszawa",
    navigationAddress: "Hoża 51A, Warszawa",
    beers: [
      Beer(name: "Kosmiczne Czarownice Sour", price: 7.99, communityVotes: 30),
      Beer(
          name: "Warkocze Wikingów Brown Ale", price: 6.49, communityVotes: 16),
      Beer(
          name: "Słonecznikowy Nektar Wheat Beer",
          price: 5.99,
          communityVotes: 11),
    ],
  ),
];

List<GoodBeer> goodBeers = [
  GoodBeer('Kosmiczne Czarownice Sour', 'Jabeerwocky'),
  GoodBeer('Gorzka Wódka IPA', 'Kufle i Kapsle'),
  GoodBeer('Złoty Pociąg Pilsner', 'Piwoteka Warszawska')
];


